# -*- coding: utf-8 -*-

"""
============================================
Test 3 trackbar
--------------------------------------------

Correction du flou de bougé appliqué à une 
image de Stéréopolis (florangerie1.png)

--------------------------------------------
@auth: Lâmân Lelégard
@date: 18/01/2021
============================================
"""

import cv2 as cv 
import numpy as np

#--------------------------------------> test
def test():
    """
    -----------------------------------
    Test trackbar
    -----------------------------------
    """
    
    tw = 'Deconvolution de Wiener'
    cv.namedWindow(tw)
    tn = 'Taille du flou\n(en pixels)' 
    te = 'Regularisation' 
    cv.createTrackbar(tn,tw,1,100,change)
    cv.createTrackbar(te,tw,1,100,change)
    
    img1 = cv.imread('florangerie1.png')
    tf_img1 = np.fft.fft2(img1.astype('float32'))
    tf_img1 = np.fft.fftshift(tf_img1)
    
    img3 = img1
    
    # Domaine de définition du filtre de Wiener (bourrin)
    # N'HÉSITEZ PAS "OPTIMISER" CETTE HORREUR ! ↓↓↓
    imgx = 0.*img1
    kc = 0
    ky = 0
    kx = 0
    Nx = imgx.shape[1]
    while kc < imgx.shape[2]:
        while ky < imgx.shape[0]:
            while kx < imgx.shape[1]:
                imgx[ky,kx,kc] = float(kx - np.floor(Nx/2.))
                imgx[ky,kx,kc] /= float(Nx)
                kx += 1
            kx = 0
            ky += 1
        ky = 0
        kc += 1
    
    # t1 et t2 : bidouilles pour fermer la fenêtre
    t1 = cv.getWindowProperty(tw,cv.WND_PROP_VISIBLE) >= 1
    t2 = (cv.waitKey(100) & 0xFF) == ord('q')
    
    # Ajout de texte :
    img2 = np.zeros((25,img1.shape[1],3),np.uint8)
    img2 [:] = [231,235,239]
    tx1 = 'Presser "q" pour quitter - ' # Texte à afficher
    tx1 += '(quand la "croix" ne fonctionne pas)'
    tx2 = (10,15)                       # Origine du texte
    tx3 = cv.FONT_HERSHEY_SIMPLEX       # Police d'écriture
    tx4 = 0.4                           # Taille  de police 
    tx5 = (0,0,0)                       # Couleur de police
    tx6 = 1                             # Épaisseur
    tx7 = cv.LINE_AA                    # Type de ligne (?)
    cv.putText(img2,tx1,tx2,tx3,tx4,tx5,tx6,tx7)
    
    while t1:
        
        cv.imshow(tw,np.vstack((img2,img3)))
        
        if t2:
            cv.destroyAllWindows()
            break
        
        pn = cv.getTrackbarPos(tn,tw)
        pe = cv.getTrackbarPos(te,tw)
        
        # Déconvolution de Wiener (dans Fourier) :
        imgw = np.sinc(imgx*float(pn))
        imgw = imgw/(imgw*imgw + 1./(pe*pe))
        imgw = tf_img1*imgw
        imgw = np.fft.ifftshift(imgw)
        imgw = np.fft.ifft2(imgw)
        imgw = np.abs(imgw)
        img3 = imgw.astype('uint8')
        
        # t1 et t2 : bidouilles pour fermer la fenêtre
        t1 = cv.getWindowProperty(tw,cv.WND_PROP_VISIBLE) >= 1
        t2 = (cv.waitKey(1) & 0xFF) == ord('q')


#--------------------------------------> change (?)
def change(x):
    # print(x)
    pass


test()


