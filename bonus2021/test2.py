# -*- coding: utf-8 -*-

"""
============================================
Test 2 trackbar
--------------------------------------------

Flou de bougé appliqué à Bip-Bip

--------------------------------------------
@auth: Lâmân Lelégard
@date: 18/01/2021
============================================
"""

import cv2 as cv 
import numpy as np

#--------------------------------------> test
def test():
    """
    -----------------------------------
    Test trackbar
    -----------------------------------
    """
    
    cv.namedWindow('BipBip !')
    n1 = 'Taille du flou\n(en pixels)' 
    cv.createTrackbar(n1,'BipBip !',0,100,change)
    
    img1 = cv.imread('bipbip1.jpg')
    img3 = img1
    img0 = image_integrale(img1)
    
    # t1 et t2 : bidouilles pour fermer la fenêtre
    t1 = cv.getWindowProperty('BipBip !',cv.WND_PROP_VISIBLE) >= 1
    t2 = (cv.waitKey(100) & 0xFF) == ord('q')
    
    # Ajout de texte :
    img2 = np.zeros((25,img1.shape[1],3),np.uint8)
    img2 [:] = [231,235,239]
    tx1 = 'Presser "q" pour quitter - ' # Texte à afficher
    tx1 += '(quand la "croix" ne fonctionne pas)'
    tx2 = (10,15)                       # Origine du texte
    tx3 = cv.FONT_HERSHEY_SIMPLEX       # Police d'écriture
    tx4 = 0.4                           # Taille  de police 
    tx5 = (0,0,0)                       # Couleur de police
    tx6 = 1                             # Épaisseur
    tx7 = cv.LINE_AA                    # Type de ligne (?)
    cv.putText(img2,tx1,tx2,tx3,tx4,tx5,tx6,tx7)
    
    while t1:
        
        cv.imshow('BipBip !',np.vstack((img2,img3)))
        
        if t2:
            cv.destroyAllWindows()
            break

        n = cv.getTrackbarPos(n1,'BipBip !')

        if n > 0:
            # ↓↓↓ LES 5 LIGNES IMPORTANTES ! ↓↓↓
            img3 = np.zeros((img0.shape[0],n,3)).astype('uint32')
            img3 = np.hstack((img3,img0[:,:-n,:]))
            img3 = img0.astype('float32')-img3.astype('float32')
            img3 = img3/float(n)
            img3 = img3.astype('uint8')
        else:
            img3 = img1
            
        # t1 et t2 : bidouilles pour fermer la fenêtre
        t1 = cv.getWindowProperty('BipBip !',cv.WND_PROP_VISIBLE) >= 1
        t2 = (cv.waitKey(1) & 0xFF) == ord('q')


#--------------------------------------> change (?)
def change(x):
    # print(x)
    pass

#--------------------------------------> image_integrale
def image_integrale(img):
    imgi = img.astype('uint32')
    kc = 0
    ky = 0
    kx = 1
    while kc < img.shape[2]:
        while ky < img.shape[0]:
            while kx < img.shape[1]:
                imgi[ky,kx,kc] += imgi[ky,kx-1,kc]
                kx += 1
            kx = 1
            ky += 1
        ky = 0
        kc += 1
    return imgi

test()


