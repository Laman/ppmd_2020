# -*- coding: utf-8 -*-
"""
-----------------------
Test pour cours PPMD
-----------------------
Calcul flou moyen : 
    - convolution 
    - séparabilité
    - Fourier
    - performances ?
-----------------------
@auth: Lâmân Lelégard
@date: 16/01/2017
-----------------------
"""

import time
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from scipy import ndimage as im
from math import pi


#--------------------------------------> test
def test():
    """
    -----------------------------------
    1°/ Test d'ouverture d'image
    2°/ Convolution filtre moyen
    3°/ Séparabilité filtre moyen
    4°/ Filtre moyen en FFT
    -----------------------------------
    """
    
    print("\nLecture et ouverture d'image")
    tic = time.time()
    img1 = io.imread('siemens.png')
    plt.figure(1)
    plt.title('Image originale')
    plt.imshow(img1)
    toc = time.time()
    print("Ouverture/affichage : ",toc-tic,"secondes")
    
    print(" ")
    n = int(input("Taille du filtre : "))
    print(" ")
    
    tic = time.time()
    img3 = flou1(img1,n,n)
    plt.figure(2)
    plt.title('Image floue 1')
    plt.imshow(img3)
    toc = time.time()
    print("Filtre complet : ",toc-tic,"secondes")
    
    tic = time.time()
    img4 = flou2(img1,n,n)
    plt.figure(3)
    plt.title('Image floue 2')
    plt.imshow(img4)
    toc = time.time()
    print("Filtre séparé : ",toc-tic,"secondes")
    
    tic = time.time()
    img5 = flou3(img1,n,n)
    plt.figure(4)
    plt.title('Image floue 3')
    plt.imshow(img5)
    toc = time.time()
    print("Filtre séparé : ",toc-tic,"secondes")
    
    tic = time.time()
    img6 = flou4(img1,n,n)
    plt.figure(5)
    plt.title('Image floue 4')
    plt.imshow(img6)
    toc = time.time()
    print("Filtre Fourier : ",toc-tic,"secondes")
    
    


#--------------------------------------> flou1
def flou1(img,nx,ny):
    """
    -----------------------------------
    INPUT :
            img = image à flouter
            nx  = demi-largeur du flou
            ny  = demi-hauteur du flou
    OUTPUT :
            image floue (filtre moyen)
    -----------------------------------
    """
    
    im_x = img.shape[???]
    im_y = img.shape[???]
    im_c = img.shape[???]
    
    imgf = 0*img
    
    w = ((2*nx+1)*(2*ny+1))
    
    for ic in range(???):
        for ix in range(???):
            for iy in range(???):
                
                tmp = 0
                
                for jx in range(-nx,nx+1):
                    for jy in range(-ny,ny+1):
                        
                        val = img[iy+jy,ix+jx,ic]
                        tmp = tmp + val
                        
                imgf[iy,ix,ic] = ???
                
    return imgf
    
#--------------------------------------> flou2
def flou2(img,nx,ny):
    """
    -----------------------------------
    INPUT :
            img = image à flouter
            nx  = demi-largeur du flou
            ny  = demi-hauteur du flou
    OUTPUT :
            image floue (filtre moyen)
    OTHER  :
            flou1(img,nx,ny)
    -----------------------------------
    """
    
    imgf = flou1(img,???,???)
    imgf = flou1(???,???,???)
    
    return imgf
    

#--------------------------------------> flou3
def flou3(img,nx,ny):
    """
    -----------------------------------
    INPUT :
            img = image à flouter
            nx  = demi-largeur du flou
            ny  = demi-hauteur du flou
    OUTPUT :
            image floue (filtre moyen)
    OTHER  :
            im.convolve()
    -----------------------------------
    """
    
    fx = ???
    fy = ???
    
    imgf = 0*img;
    
    for ic in range(img.shape[2]):
        imgf[:,:,ic] = im.convolve(img[:,:,ic],fx)
        imgf[:,:,ic] = im.convolve(imgf[:,:,ic],fy)
    
    return imgf
    
#--------------------------------------> flou4
def flou4(img,nx,ny):
    """
    -----------------------------------
    INPUT :
            img = image à flouter
            nx  = demi-largeur du flou
            ny  = demi-hauteur du flou
    OUTPUT :
            image floue (filtre moyen)
    OTHER  :
            np.fft2()
    -----------------------------------
    """
    
    imgf = 0*img
    
    sinc_x = np.array([range(img.shape[???])])
    sinc_x = sinc_x/img.shape[???]
    sinc_x = (2*sinc_x - 1)/2
    sinc_x = np.sinc((2*nx+1)*sinc_x)
    sinc_x = np.kron(np.ones((img.shape[???],???)),sinc_x)
    
    sinc_y = np.array([range(img.shape[???])])
    sinc_y = sinc_y/img.shape[???]
    sinc_y = (2*sinc_y - 1)/2
    sinc_y = np.sinc((2*ny+1)*sinc_y)
    sinc_y = np.kron(np.ones((???,img.shape[???])),sinc_y.T)
    
    for k in range(img.shape[2]):
        tf_img = np.fft.fft2(img[:,:,k])
        tf_img = np.fft.fftshift(tf_img)
        tf_img = ???*???*tf_img
        tf_img = np.fft.ifftshift(tf_img)
        tf_img = np.fft.ifft2(tf_img)
        imgf[:,:,k] = np.real(tf_img)
    
    return imgf

